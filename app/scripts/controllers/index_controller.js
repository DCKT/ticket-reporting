Reporting.IndexController = Ember.ArrayController.extend({
	sortProperties: ['createdAt'],
	sortAscending: false,
	actions: {
		createTicket: function() {
			var ticket = {
				title: this.get('title'),
				number: this.get('number'),
				time: this.get('time'),
				createdAt: moment().format("D/M/YYYY, h:mm:ss a"),
				updatedAt: moment().format("D/M/YYYY, h:mm:ss a"),
				editing: false
			};

			var newTicket = this.store.createRecord('ticket', ticket);
			this.set('title', '');
			this.set('number', '');
			this.set('time', '');
			newTicket.save();
		},
		deleteTicket: function(element) {
			element.deleteRecord();
			element.save();
		},
		editTicket: function(element) {
			element.set('isEditing', true);
		},
		changeTicket: function(element) {
			element.set('updatedAt',  moment().format("D/M/YYYY, h:mm:ss a"));
			element.save();
			element.set('isEditing', false);
		},
		cancelEditing: function(element) {
			element.set('isEditing', false);
		},
		copyToClipboard: function(element) {
			var string = "#"+ element.get('number') +" - "+ element.get('title');
			prompt("You can use Ctrl + C", string);
		}
	}
});