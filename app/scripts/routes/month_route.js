Reporting.MonthRoute = Ember.Route.extend({
  model: function(id) {
    var month = "2014-"+ id.month_id;
    var dayNumber = moment(month, "YYYY-M").daysInMonth();
    var calendar = [[], [], [], [], [], [], [], []];
    var firstDay;

    for (var i = 0; i < dayNumber; i++) {
      if (i === 0)
        firstDay = moment(month, "YYYY-M").date(i + 1).day();

      calendar[moment(month, "YYYY-M").date(i + 1).day()].push(Reporting.Day.create({
          number: i + 1
        })
      );
    }

    for (var i = 0; i < firstDay; i++) {
      calendar[i].back = true;
    }

    return calendar;
  }
});
