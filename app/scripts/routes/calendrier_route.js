Reporting.CalendrierIndexRoute = Ember.Route.extend({
  model: function() {
    var dayNumber = moment().daysInMonth();
    var calendar = [[], [], [], [], [], [], [], []];

    for (var i = 0; i < dayNumber; i++) {
      if (i === 0) {
          calendar[moment().date(i + 1).day()].firstDay = true;
      }
      calendar[moment().date(i + 1).day()].push(Reporting.Day.create({
          number: i + 1
        })
      );
    }

    return calendar;
  }
});
