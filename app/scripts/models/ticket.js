Reporting.Ticket = DS.Model.extend({
	title: DS.attr(),
	time: DS.attr(),
	number: DS.attr(),
	editing: DS.attr(),
	createdAt: DS.attr(),
	updatedAt: DS.attr()
});