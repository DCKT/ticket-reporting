Reporting.Day = Ember.Object.extend({
  number: null,
  isFirstDay: function() {
    if (this.get('number') == 1) {
      return true;
    }
  }.property('number')
});
